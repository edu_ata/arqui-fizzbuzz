var assert = require('assert');
var expect = require('chai').expect;
var should = require('chai').should();

import fizzbuzz1 from '../fizzbuzz.js';
//import Province from '../province.js';

describe('FizzBuzz', function() {
   //let asia;
   //beforeEach(function() {
   //  asia = new Province(sampleProvinceData());
   //});

   it('deberia retornar el mismo numero si no es multiplo de 3 o 5', function() {
     expect(fizzbuzz1(4)).equal("4");
     expect(fizzbuzz1(7)).equal("7");
   });

   it('deberia retornar Fizz si es multiplo de 3', function() {
    expect(fizzbuzz1(3)).equal("Fizz");
    expect(fizzbuzz1(9)).equal("Fizz");
    expect(fizzbuzz1(21)).equal("Fizz");
   });

   it('deberia retornar Buzz si es multiplo de 5', function() {
    expect(fizzbuzz1(5)).equal("Buzz");
    expect(fizzbuzz1(20)).equal("Buzz");
    expect(fizzbuzz1(40)).equal("Buzz");
   });

   it('deberia retornar FizzBuzz si es multiplo de 3 y 5', function() {
    expect(fizzbuzz1(15)).equal("FizzBuzz");
    expect(fizzbuzz1(30)).equal("FizzBuzz");
    expect(fizzbuzz1(45)).equal("FizzBuzz");
   });

   it('deberia retornar error si el numero es menor a 0', function() {
    expect(fizzbuzz1(-2)).equal("error");
   });

   





});
