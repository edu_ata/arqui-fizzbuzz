export default fizzbuzz1;
/*
function fizzbuzz () {
  for(i = 0; i < 100; i++) 
  {
    console.log(fizzbuzz1(i));
  }
}*/

function fizzbuzz1(number) {
  if (number < 0) {
    return "error"
  }
  if (isFizz(number) && isBuzz(number)) {
    return "FizzBuzz";
  }
  if (isFizz(number)) {
    return "Fizz";
  }
  if (isBuzz(number)) {
    return "Buzz";
  }
  return number.toString();
}

function isFizz(number) {
  return (number % 3 === 0);
}

function isBuzz(number) {
  return number % 5 === 0;
}

